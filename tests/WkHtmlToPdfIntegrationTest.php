<?php

namespace Ciltocruz\WkhtmltopdfPhpLib\Tests;

use Ciltocruz\WkhtmltopdfPhpLib\Exception\WkHtmlException;
use Ciltocruz\WkhtmltopdfPhpLib\WkHtmlToPdf;

/**
 * @coversDefaultClass  Ciltocruz\WkhtmltopdfPhpLib
 */
class WkHtmlToPdfIntegrationTest extends UtilsTest
{
    public const USERNAME = '--username';
    private WkHtmlToPdf $wkhtmltopdf;

    public function setUp(): void
    {
        $_SERVER['SERVER_NAME'] = 'localhost';
        $this->wkhtmltopdf = new WkHtmlToPdf('');
    }

    public function tearDown(): void
    {
        if ($this->wkhtmltopdf->getTmp() !== '' && file_exists($this->wkhtmltopdf->getTmp())) {
            unlink($this->wkhtmltopdf->getTmp());
        }
    }

    /**
     * @throws WkHtmlException
     * @covers \Ciltocruz\WkhtmltopdfPhpLib\WkHtmlToPdf
     * @covers \Ciltocruz\WkhtmltopdfPhpLib\Exception\WkHtmlException
     */
    public function testShouldGetPdfFileContent_whenOutputAsString(): void
    {
        //given

        //when
        $this->wkhtmltopdf->set_html($this->getHtml());
        $this->wkhtmltopdf->render();
        $ret = $this->wkhtmltopdf->getPdf();
        $pdfString = $this->wkhtmltopdf->output(WkHtmlToPdf::$PDF_ASSTRING, "myfile.pdf");

        //then
        $this->assertEquals($pdfString, $ret);
    }

    /**
     * @throws WkHtmlException
     * @covers \Ciltocruz\WkhtmltopdfPhpLib\WkHtmlToPdf
     * @covers \Ciltocruz\WkhtmltopdfPhpLib\Exception\WkHtmlException
     */
    public function testShouldGetPdfFile_whenOutputAsDownload(): void
    {
        //given

        //when
        $this->wkhtmltopdf->set_html($this->getHtml());
        $this->wkhtmltopdf->render();
        $ret = $this->wkhtmltopdf->getPdf();
        $this->wkhtmltopdf->output(WkHtmlToPdf::$PDF_SAVEFILE, "src/tmp/myfile.pdf");
        $string = file_get_contents("src/tmp/myfile.pdf");

        //then
        $this->assertEquals($string, $ret);

        unlink("src/tmp/myfile.pdf");
    }

    /**
     * @throws WkHtmlException
     * @covers \Ciltocruz\WkhtmltopdfPhpLib\WkHtmlToPdf
     * @covers \Ciltocruz\WkhtmltopdfPhpLib\Exception\WkHtmlException
     */
    public function testShouldThrownException_whenOutputAsInvalidMode(): void
    {
        //given
        $this->expectException(WkHtmlException::class);

        //when
        $this->wkhtmltopdf->set_html($this->getHtml());
        $this->wkhtmltopdf->render();
        $this->wkhtmltopdf->output("INVALID", "src/tmp/myfile.pdf");
    }


    public function pdfModes(): array
    {
        return array(
            array(WkHtmlToPdf::$PDF_DOWNLOAD),
            array(WkHtmlToPdf::$PDF_EMBEDDED)
        );
    }

    /**
     * @covers \Ciltocruz\WkhtmltopdfPhpLib\WkHtmlToPdf
     * @throws WkHtmlException
     * @runInSeparateProcess
     * @dataProvider pdfModes
     */
    public function testShouldCreatePDF_whenModeIsSelected(String $mode): void
    {
        //given
        $this->expectOutputRegex("/^%PDF-1./");

        //when
        $this->wkhtmltopdf->set_html($this->getHtml());
        $this->wkhtmltopdf->render();
        $this->wkhtmltopdf->output($mode, "src/tmp/myfile.pdf");
    }


    /**
     * @covers \Ciltocruz\WkhtmltopdfPhpLib\WkHtmlToPdf
     * @throws WkHtmlException
     * @covers \Ciltocruz\WkhtmltopdfPhpLib\Exception\WkHtmlException
     * @dataProvider pdfModes
     */
    public function testShouldThrownException_whenDownloadMode(String $mode): void
    {
        //given
        $this->expectException(WkHtmlException::class);

        //when
        $this->wkhtmltopdf->set_html($this->getHtml());
        $this->wkhtmltopdf->render();
        $this->wkhtmltopdf->output($mode, "src/tmp/myfile.pdf");
    }


    public function providersOS(): array
    {
        return array(
            array('Linux'),
            array('Windows')
        );
    }

    /**
     * @throws WkHtmlException
     * @covers       \Ciltocruz\WkhtmltopdfPhpLib\WkHtmlToPdf
     */
    public function testShouldCreatePdfWithAnyWebsite_whenAsStringMode(): void
    {
        //given
        $this->expectOutputRegex("/^%PDF-1./");

        //when
        $this->wkhtmltopdf->set_html("https://www.google.com");
        $this->wkhtmltopdf->render();
        $this->wkhtmltopdf->output(WkHtmlToPdf::$PDF_ASSTRING, "src/tmp/myfile.pdf");
        echo $this->wkhtmltopdf->getPdf();
    }
}
