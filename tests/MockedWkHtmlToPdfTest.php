<?php

namespace Ciltocruz\WkhtmltopdfPhpLib {

    function proc_open($command, array $descriptor_spec, &$pipes, ?string $cwd, ?array $env_vars, ?array $options)
    {
        global $mockSocketCreate;
        if (isset($mockSocketCreate) && $mockSocketCreate === true) {
            return false;
        }

        return \proc_open($command, $descriptor_spec, $pipes, $cwd, $env_vars, $options);
    }
}

namespace Ciltocruz\WkhtmltopdfPhpLib\Tests {

    use Ciltocruz\WkhtmltopdfPhpLib\WkHtmlToPdf;
    use PHPUnit\Framework\TestCase;
    use function PHPUnit\Framework\assertEquals;

    class MockedWkHtmlToPdfTest extends TestCase
    {
        public function setUp(): void
        {
            global $mockSocketCreate;
            $mockSocketCreate = false;
        }

        /**
         * This will enable the mock and call the code. socket_create will now
         * return false instead the call of the original socket_create, our work
         * here is done.
         * @test
         * @covers \Ciltocruz\WkhtmltopdfPhpLib\WkHtmlToPdf
         */
        public function shouldReturnEmpty_whenProcOpenFails(): void
        {
            global $mockSocketCreate;
            $mockSocketCreate = true;
            $wkhtmltopdf = new WkHtmlToPdf('');
            $stdout = $wkhtmltopdf->getHelp();
            assertEquals("", $stdout);
            $mockSocketCreate = false;
        }
    }
}

namespace {

    $mockSocketCreate = false;
}
