<?php

namespace Ciltocruz\WkhtmltopdfPhpLib\Tests;

use Ciltocruz\WkhtmltopdfPhpLib\WkHtmlToPdf;
use PHPUnit\Framework\TestCase;

abstract class UtilsTest extends TestCase
{
    public function getHtml(): string
    {
        return "<html><body><div><h1>Hello</h1></div></body></html>";
    }

    public function createMockForOS(string $returnValue): WkHtmlToPdf
    {
        $stub = $this->getMockBuilder(WkHtmlToPdf::class)
            ->onlyMethods(['getOSName'])
            ->disableOriginalConstructor()
            ->getMock();

        $stub
            ->method('getOSName')
            ->willReturn($returnValue);
        $stub->__construct('');

        return $stub;
    }
}
