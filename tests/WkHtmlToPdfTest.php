<?php

namespace Ciltocruz\WkhtmltopdfPhpLib\Tests;

use Ciltocruz\WkhtmltopdfPhpLib\Exception\WkHtmlException;
use Ciltocruz\WkhtmltopdfPhpLib\WkHtmlToPdf;

/**
 * @coversDefaultClass  Ciltocruz\WkhtmltopdfPhpLib\
 */
class WkHtmlToPdfTest extends UtilsTest
{
    public const USERNAME = '--username';
    private WkHtmlToPdf $wkhtmltopdf;

    public function setUp(): void
    {
        $_SERVER['SERVER_NAME'] = 'localhost';
        $this->wkhtmltopdf = new WkHtmlToPdf('');
    }

    public function tearDown(): void
    {
        if ($this->wkhtmltopdf->getTmp() !== '' && file_exists($this->wkhtmltopdf->getTmp())) {
            unlink($this->wkhtmltopdf->getTmp());
        }
    }

    /**
     * @covers \Ciltocruz\WkhtmltopdfPhpLib\WkHtmlToPdf
     */
    public function testGetBasePathSite(): void
    {
        $wk = new WkHtmlToPdf();

        $this->assertEquals("https://localhost/", $wk->WKPDF_BASE_SITE);
    }

    /**
     * @covers \Ciltocruz\WkhtmltopdfPhpLib\WkHtmlToPdf
     */
    public function testGetCmdOnWindows(): void
    {
        $stub = $this->createMockForOS("Windows");

        $this->assertStringContainsString("/bin/wkhtmltopdf.exe", $stub->getCmd());
    }

    /**
     * @covers \Ciltocruz\WkhtmltopdfPhpLib\WkHtmlToPdf
     */
    public function testGetCmdOnLinux(): void
    {
        $stub = $this->createMockForOS("Linux");

        $this->assertEquals("src/../bin/wkhtmltopdf64", $stub->getCmd());
    }

    /**
     * @covers \Ciltocruz\WkhtmltopdfPhpLib\WkHtmlToPdf
     */
    public function testshouldGetHelp_whenInvoked(): void
    {
        $this->assertStringContainsString("(with patched qt)", $this->wkhtmltopdf->getHelp());
    }

    /**
     * @covers \Ciltocruz\WkhtmltopdfPhpLib\WkHtmlToPdf
     */
    public function testAddArgs(): void
    {
        $this->wkhtmltopdf->args_add(self::USERNAME, 'user');
        self::assertCount(1, $this->wkhtmltopdf->getArgs());
    }

    /**
     * @covers \Ciltocruz\WkhtmltopdfPhpLib\WkHtmlToPdf
     */
    public function testShouldGetNumberOfCopies_whenCopiesIsAssigned(): void
    {
        $this->wkhtmltopdf->set_copies(2);
        self::assertEquals(2, $this->wkhtmltopdf->getCopies());
    }

    /**
     * @covers \Ciltocruz\WkhtmltopdfPhpLib\WkHtmlToPdf
     */
    public function testShould0Args_whenRemoveArg(): void
    {
        $this->wkhtmltopdf->args_add(self::USERNAME, 'user');
        $this->wkhtmltopdf->args_remove(self::USERNAME);
        self::assertCount(0, $this->wkhtmltopdf->getArgs());
    }

    /**
     * @throws WkHtmlException
     * @covers \Ciltocruz\WkhtmltopdfPhpLib\WkHtmlToPdf
     */
    public function testShouldCreateTemporalFile_whenSetHtml(): void
    {
        //given

        //when
        $this->wkhtmltopdf->set_html($this->getHtml());

        //then
        self::assertStringContainsString("tmp/", $this->wkhtmltopdf->getUrl());
    }

    /**
     * @throws WkHtmlException
     * @covers \Ciltocruz\WkhtmltopdfPhpLib\WkHtmlToPdf
     */
    public function testShouldCreateFile_whenRenderHtml(): void
    {
        //given

        //when
        $this->wkhtmltopdf->set_html($this->getHtml());

        //then
        self::assertTrue($this->wkhtmltopdf->render());
    }

    /**
     * @throws WkHtmlException
     * @covers \Ciltocruz\WkhtmltopdfPhpLib\WkHtmlToPdf
     */
    public function testShouldCreateFileWithArgs_whenRenderHtml(): void
    {
        //given
        $this->wkhtmltopdf->set_orientation(WkHtmlToPdf::$PDF_LANDSCAPE);
        $this->wkhtmltopdf->set_page_size("A3");
        $this->wkhtmltopdf->set_title("myTitle");
        $this->wkhtmltopdf->set_grayscale(true);

        //when
        $this->wkhtmltopdf->args_add(self::USERNAME, 'user');
        $this->wkhtmltopdf->set_html($this->getHtml());
        $renderStatus = $this->wkhtmltopdf->render();

        //%PDF-1.4
        $this->expectOutputRegex("/^%PDF-1./");
        echo $this->wkhtmltopdf->getPdf();

        //then
        self::assertEquals(
            [
                self::USERNAME => 'user',
            ],
            $this->wkhtmltopdf->getArgs()
        );
        $this->assertTrue($renderStatus);
    }

    /**
     * @throws WkHtmlException
     * @covers \Ciltocruz\WkhtmltopdfPhpLib\WkHtmlToPdf
     * @covers \Ciltocruz\WkhtmltopdfPhpLib\Exception\WkHtmlException
     */
    public function testShouldThrowException_whenRenderHtmlAlreadyRendered(): void
    {
        //given
        $this->expectException(WkHtmlException::class);
        //when
        $this->wkhtmltopdf->set_html($this->getHtml());
        $this->wkhtmltopdf->set_html($this->getHtml());
        //then
    }

    /**
     * @throws WkHtmlException
     * @covers \Ciltocruz\WkhtmltopdfPhpLib\WkHtmlToPdf
     * @covers \Ciltocruz\WkhtmltopdfPhpLib\Exception\WkHtmlException
     */
    public function testShouldThrowException_whenRenderHtmlisEmpty(): void
    {
        //given
        $this->expectException(WkHtmlException::class);
        //when
        $this->wkhtmltopdf->set_html("");
        //then
    }

    /**
     * @throws WkHtmlException
     * @covers \Ciltocruz\WkhtmltopdfPhpLib\WkHtmlToPdf
     * @covers \Ciltocruz\WkhtmltopdfPhpLib\Exception\WkHtmlException
     */
    public function testShouldThrowError_whenCmdIsWrong(): void
    {
        //given
        $this->expectException(WkHtmlException::class);
        $this->wkhtmltopdf->setcmd("wrongCmd");
        //when
        $this->wkhtmltopdf->set_html("asdf");
        //then
        $this->wkhtmltopdf->render();
    }

    /**
     * @throws WkHtmlException
     * @covers \Ciltocruz\WkhtmltopdfPhpLib\WkHtmlToPdf
     * @covers \Ciltocruz\WkhtmltopdfPhpLib\Exception\WkHtmlException
     */
    public function testShouldThrowError_whenUrlIs404(): void
    {
        //given
        $this->expectException(WkHtmlException::class);

        //when
        $this->wkhtmltopdf->args_add(self::USERNAME, 'user');
        $this->wkhtmltopdf->set_url("https://www.dontexistswithhomer.net");
        //then
        $this->wkhtmltopdf->render();
    }
}
