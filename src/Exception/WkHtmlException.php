<?php

namespace Ciltocruz\WkhtmltopdfPhpLib\Exception;

use Exception;

class WkHtmlException extends Exception
{
    public function __construct(string $message, int $code = 0)
    {
        parent::__construct($message, $code);
    }
}
