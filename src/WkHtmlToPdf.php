<?php

namespace Ciltocruz\WkhtmltopdfPhpLib;

use Ciltocruz\WkhtmltopdfPhpLib\Exception\WkHtmlException;
use Exception;

class WkHtmlToPdf
{
    /**
     * protected use variables.
     */
    protected string $cmd = '';
    protected string $tmp = '';
    protected string $pdf = '';
    protected string $status = '';
    protected string $orient = 'Portrait';
    protected string $size = 'A4';
    protected int $copies = 1;
    protected bool $grayscale = false;
    protected string $title = '';
    protected array $args = [];
    protected string $url = '';
    public ?string $WKPDF_BASE_SITE = null;
    private string $WKPDF_BIN_PATH;
    private string $WKPDF_TMP_PATH;

    /**
     * Constructor: initialize command line and reserve temporary file.
     * @param string|null $wkpdfBaseSite
     */
    public function __construct(string $wkpdfBaseSite = null)
    {
        if ($wkpdfBaseSite === null) {
            $this->WKPDF_BASE_SITE = 'https://' . $_SERVER['SERVER_NAME'] . '/';
        } else {
            $this->WKPDF_BASE_SITE = $wkpdfBaseSite;
        }

        $WKPDF_BASE_PATH = str_replace(
            str_replace('\\', '/', getcwd() . '/'),
            '',
            dirname(str_replace('\\', '/', __FILE__))
        ) . '/';
        $this->WKPDF_BIN_PATH = $WKPDF_BASE_PATH . "../bin/";
        $this->WKPDF_TMP_PATH = $WKPDF_BASE_PATH . "tmp/";

        $this->cmd = $this->_getCMD();
    }

    /**
     * @return string
     */
    public function getPdf(): string
    {
        return $this->pdf;
    }

    /**
     * @return int
     */
    public function getCopies(): int
    {
        return $this->copies;
    }

    /**
     * @return array
     */
    public function getArgs(): array
    {
        return $this->args;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * Advanced execution routine.
     *
     * @param string $cmd The command to execute.
     * @param string $input Any input not in arguments.
     * @return array An array of execution data; stdout, stderr and return "error" code.
     */
    protected static function _pipeExec(string $cmd, string $input = ''): array
    {
        $proc = proc_open(
            $cmd,
            [0 => ['pipe', 'r'], 1 => ['pipe', 'w'], 2 => ['pipe', 'w']],
            $pipes,
            null,
            null,
            ['suppress_errors' => true]
        );
        if (!$proc) {
            return ['stdout' => '', 'stderr' => 'CreateProcess failed.', 'return' => 1];
        }
        fwrite($pipes[0], $input);
        fclose($pipes[0]);
        $stdout = stream_get_contents($pipes[1]); // max execution time exceeded issue
        fclose($pipes[1]);
        $stderr = stream_get_contents($pipes[2]);
        fclose($pipes[2]);
        $rtn = proc_close($proc);

        return [
            'stdout' => $stdout,
            'stderr' => $stderr,
            'return' => $rtn,
        ];
    }

    /**
     * Function that attempts to return the correct executable path.
     *
     * @return string WKHTMLTOPDF path.
     */
    protected function _getCMD(): string
    {
        if ($this->cmd === '') {
            $this->cmd = $this->WKPDF_BIN_PATH . ($this->getOSName() === 'Windows' ? 'wkhtmltopdf.exe' : 'wkhtmltopdf64');
        }

        return $this->cmd;
    }

    /**
     * Force the client to download PDF file when finish() is called.
     */
    public static string $PDF_DOWNLOAD = 'D';
    /**
     * Returns the PDF file as a string when finish() is called.
     */
    public static string $PDF_ASSTRING = 'S';
    /**
     * When possible, force the client to embed PDF file when finish() is called.
     */
    public static string $PDF_EMBEDDED = 'I';
    /**
     * PDF file is saved into the server space when finish() is called. The path is returned.
     */
    public static string $PDF_SAVEFILE = 'F';
    /**
     * PDF generated as landscape (vertical).
     */
    public static string $PDF_PORTRAIT = 'Portrait';
    /**
     * PDF generated as landscape (horizontal).
     */
    public static string $PDF_LANDSCAPE = 'Landscape';

    /**
     * Print error in output.
     *
     * @param string $msg The error message.
     * @param array $out The output.
     * @throws WkHtmlException
     */
    protected static function _retError(string $msg, array $out): void
    {
        throw new WkHtmlException(
            $msg . '<table>' // changed from WkHtmlException to die due to stack frame error
            . '<tr><td>RESULT:</td><td><code>' . htmlspecialchars($out['return'], ENT_QUOTES) . '</code></td></tr>'
            . '<tr><td>STDERR:</td><td><code><pre>' . htmlspecialchars(
                $out['stderr'],
                ENT_QUOTES
            ) . '</pre></code></td></tr>'
            . '<tr><td>STDOUT:</td><td><code>' . htmlspecialchars($out['stdout'], ENT_QUOTES) . '</code></td></tr>'
            . '</table>'
        );
    }

    /**
     * Helper function that builds the list of arguments.<br>
     * Do not call this directly.
     *
     * @return string List of custom arguments.
     */
    protected function args_build(): string
    {
        $res = '';
        foreach ($this->args as $switch => $value) {
            $res .= ' ' . $switch . ' ' . escapeshellarg($value);
        }

        return $res;
    }

    /**
     * In case where platform detection fails (or you want to disable it), this function is able to let you use your own path to the executable.
     *
     * @param string $cmd The command line pointing to the WKHTMLTOPDF executable.
     */
    public function setcmd(string $cmd): void
    {
        $this->cmd = $cmd;
    }

    /**
     * @return string
     */
    public function getCmd(): string
    {
        return $this->cmd;
    }

    /**
     * Set orientation, use constants from this class.
     * By default, orientation is portrait.
     *
     * @param string $mode Use constants from this class.
     */
    public function set_orientation(string $mode): void
    {
        $this->orient = $mode;
    }

    /**
     * Set page/paper size.
     * By default, page size is A4.
     *
     * @param string $size Formal paper size (eg; A4, letter...)
     */
    public function set_page_size(string $size): void
    {
        $this->size = $size;
    }

    /**
     * Set the number of copies to be printed.
     * By default, it is one.
     *
     * @param int $count Number of page copies.
     */
    public function set_copies(int $count): void
    {
        $this->copies = $count;
    }

    /**
     * Whether to print in grayscale or not.
     * By default, it is OFF.
     */
    public function set_grayscale(bool $mode): void
    {
        $this->grayscale = $mode;
    }

    /**
     * Set PDF title. If empty, HTML <title> of first document is used.
     * By default, it is empty.
     */
    public function set_title(string $text): void
    {
        $this->title = $text;
    }

    /**
     * Set html content.
     *
     * @param string $html New html content. It *replaces* any previous content.
     * @throws WkHtmlException
     * @throws Exception
     */
    public function set_html(string $html): void
    {
        if ($this->tmp !== '') {
            throw new WkHtmlException('WKPDF html has already been set.');
        }

        if (str_starts_with($html, "http") === false) {
            do {
                $this->tmp = $this->WKPDF_TMP_PATH . random_int(0, PHP_INT_MAX) . '.html';
            } while (file_exists($this->tmp));
            //Esto se ejecuta buscando la ruta DESDE el archivo donde se ha llamado.
            if (!file_put_contents($this->tmp, $html)) {
                throw new WkHtmlException('WKPDF write temporary file failed.');
            }

            $this->url = $this->WKPDF_BASE_SITE . $this->WKPDF_TMP_PATH . basename($this->tmp);
        } else {
            $this->url = $html;
        }
    }

    /**
     * Use a URL/path instead of HTML source.
     *
     * @param string $url Fully qualified URL or absolute path.
     */
    public function set_url(string $url): void
    {
        $this->url = $url;
    }

    /**
     * Attempts to return the library's full help.
     *
     * @return string WKHTMLTOPDF HTML help.
     */
    public function getHelp(): string
    {
        $temp = self::_pipeExec('"' . $this->getCmdForExec() . '" --extended-help');

        return $temp['stdout'];
    }

    /**
     * Add custom argument to the list.
     *
     * @param string $switch Argument name (eg: --header).
     * @param string $value Argument value (eg: <b>hi</b>).
     */
    public function args_add(string $switch, string $value): void
    {
        $this->args[$switch] = $value;
    }

    /**
     * Remove an existing custom argument.
     *
     * @param string $switch Argument name (eg: --header).
     */
    public function args_remove(string $switch): void
    {
        if (isset($this->args[$switch])) {
            unset($this->args[$switch]);
        }
    }

    /**
     * Convert HTML to PDF.
     *
     * @return bool Whether successful or not.
     * @throws WkHtmlException
     */
    public function render(): bool
    {
        $pdfArrayInfo = self::_pipeExec(
            $this->getCmdForExec()
            . (($this->copies > 1) ? ' --copies ' . $this->copies : '')                // number of copies
            . ' --orientation ' . escapeshellarg($this->orient)                    // orientation
            . ' --page-size ' . escapeshellarg($this->size)                        // page size
            . ($this->grayscale ? ' --grayscale' : '')                                // grayscale
            . (($this->title !== '') ? ' --title ' . escapeshellarg($this->title) : '')    // title
            . $this->args_build()                                                // custom arguments
            . ' ' . escapeshellarg(
                $this->getOSName() !== 'Windows' ? $this->url : ($this->getUrlOrFilePath())
            ) . ' -'                                // URL and use STDOUT
        );
        if ($pdfArrayInfo['stdout'] === '') {
            self::_retError('WKPDF error ' . $pdfArrayInfo['return'] . '.', $pdfArrayInfo);
        }
        $this->status = $pdfArrayInfo['stderr'];
        if ($this->tmp !== '') {
            unlink($this->tmp);
        }

        // Here return is always 0.
        $this->pdf = $pdfArrayInfo['stdout'];

        return true;
    }

    /**
     * Return PDF with various options.
     *
     * @param string $mode How two output (constants from this same class).
     * @param string $file The PDF's filename (the usage depends on $mode.)
     * @return string|bool|int Depending on $mode, this may be success (boolean) or PDF (string).
     * @throws WkHtmlException
     */
    public function output(string $mode, string $file)
    {
        switch ($mode) {
            case self::$PDF_DOWNLOAD:
                if (!headers_sent()) {
                    header('Content-Description: File Transfer');
                    $this->setCommonHeaders();
                    // force download dialog
                    header('Content-Type: application/force-download');
                    header('Content-Type: application/octet-stream', false);
                    header('Content-Type: application/download', false);
                    header('Content-Type: application/pdf', false);
                    // use the Content-Disposition header to supply a recommended filename
                    header('Content-Disposition: attachment; filename="' . basename($file) . '";');
                    header('Content-Transfer-Encoding: binary');
                    echo $this->pdf;
                } else {
                    throw new WkHtmlException('WKPDF download headers were already sent.');
                }
                break;
            case self::$PDF_ASSTRING:
                return $this->pdf;
            case self::$PDF_EMBEDDED:
                if (!headers_sent()) {
                    header('Content-Type: application/pdf');
                    $this->setCommonHeaders();
                    header('Content-Disposition: inline; filename="' . basename($file) . '";');
                    echo $this->pdf;
                } else {
                    throw new WkHtmlException('WKPDF embed headers were already sent.');
                }
                break;
            case self::$PDF_SAVEFILE:
                return file_put_contents($file, $this->pdf);
            default:
                throw new WkHtmlException('WKPDF invalid mode "' . htmlspecialchars($mode, ENT_QUOTES) . '".');
        }

        return false;
    }

    /**
     * @return string
     */
    protected function getCmdForExec(): string
    {
        return ($this->getOSName() !== 'Windows' ? $this->cmd : 'cd ' . escapeshellarg(
            dirname($this->cmd)
        ) . ' && ' . basename($this->cmd));
    }

    /**
     * @return string
     */
    protected function getUrlOrFilePath(): string
    {
        return mb_stripos($this->url, "http") === 0 ? $this->url : '../' . str_replace(
            'vendor/ciltocruz/wkhtmltopdf-php-lib/',
            '',
            $this->url
        );
    }

    /**
     *
     */
    protected function setCommonHeaders(): void
    {
        header('Cache-Control: public, must-revalidate, max-age=0'); // HTTP/1.1
        header('Pragma: public');
        header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Content-Length: ' . mb_strlen($this->pdf));
    }

    /**
     * @return string
     */
    public function getTmp(): string
    {
        return $this->tmp;
    }

    /**
     * @return string
     */
    public function getOSName(): string
    {
        return PHP_OS_FAMILY;
    }
}
