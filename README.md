# wkhtmltopdf-php-lib

[![Stars](https://badgen.net/gitlab/stars/ciltocruz/wkhtmltopdf-php-lib)](https://app.travis-ci.com/ciltocruz/wkhtmltopdf-php-lib)
[![Travis CI Build Status](https://app.travis-ci.com/ciltocruz/wkhtmltopdf-php-lib.svg?branch=main)](https://app.travis-ci.com/ciltocruz/wkhtmltopdf-php-lib)
[![Gitlab Pipeline](https://gitlab.com/ciltocruz/wkhtmltopdf-php-lib/badges/main/pipeline.svg)](https://gitlab.com/ciltocruz/wkhtmltopdf-php-lib)
[![Gitlab coverage report](https://gitlab.com/ciltocruz/wkhtmltopdf-php-lib/badges/main/coverage.svg)](https://gitlab.com/ciltocruz/wkhtmltopdf-php-lib/-/commits/main)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=ciltocruz_wkhtmltopdf-php-lib&metric=alert_status)](https://sonarcloud.io/dashboard?id=ciltocruz_wkhtmltopdf-php-lib)
[![License: MIT](https://badgen.net//gitlab/license/ciltocruz/wkhtmltopdf-php-lib)](https://opensource.org/licenses/MIT)

[![Packagist Version](https://badgen.net/packagist/v/ciltocruz/wkhtmltopdf-php-lib)](https://packagist.org/packages/ciltocruz/wkhtmltopdf-php-lib)
[![Packagist Downloads](https://badgen.net/packagist/dt/ciltocruz/wkhtmltopdf-php-lib)](https://packagist.org/packages/ciltocruz/wkhtmltopdf-php-lib)
[![Packagist PHP Version Support](https://badgen.net/packagist/php/ciltocruz/wkhtmltopdf-php-lib)](https://packagist.org/packages/ciltocruz/wkhtmltopdf-php-lib)

[![Twitter](https://badgen.net//twitter/follow/ciltocruz)](https://www.twitter.com/ciltocruz)

HTML to PDF Converter based on wkhtmltopdf for PHP.
This code is based on [wkhtmltopdf-bindings](https://github.com/antialize/wkhtmltopdf-bindings).

This PHP library allows generating PDF Files from a url or a html page.
It uses the excellent webkit-based [wkhtmltopdf and wkhtmltoimage](http://wkhtmltopdf.org/)
available on OSX, linux, windows.

This library contains windows and linux binaries. (`0.12.6` version)
Linux binaries are based on the debian version. Verify that your system is compatible.

(*N.B.* These static binaries are extracted from  [Debian7 packages](https://github.com/h4cc/wkhtmltopdf-amd64/issues/13#issuecomment-150948179), so it might not be compatible with non-debian based linux distros)

If you detect problems you can open an issue, no problem.

## Installation using [Composer](http://getcomposer.org/)

```bash
$ composer require ciltocruz/wkhtmltopdf-php-lib
```

## Usage

### Initialization
```php
<?php

require __DIR__ . '/vendor/autoload.php';

use Ciltocruz\WkhtmltopdfPhpLib\WkHtmlToPdf;

$wkhtmltopdf = new WkHtmlToPdf();

```

### Download PDF as a file

```php
$wkhtmltopdf = new WkHtmlToPdf();
$wkhtmltopdf->set_html("your_html");
$wkhtmltopdf->render();
$wkhtmltopdf->output(WkHtmlToPdf::$PDF_DOWNLOAD, $fileOutputName);
```

### Generate local pdf file
```php
$wkhtmltopdf = new WkHtmlToPdf();
$wkhtmltopdf->set_html("your_html");
$wkhtmltopdf->render();
$wkhtmltopdf->output(WkHtmlToPdf::$PDF_SAVEFILE, $fileOutputName);
```

You can use `WkHtmlToPdf::$PDF_ASSTRING` for get the response as string.

### Pass options to wkhtmlToPdf
```php
// Type wkhtmltopdf -H to see the list of options
```

## Use this wkhtmltopdf binary in other libraries

If you want to use wkhtmltopdf in other libraries you need to add these libraries in your `composer.json`:

Some suggestions are:

```bash
$ composer require mikehaertl/phpwkhtmltopdf
$ composer require knplabs/knp-snappy
```

And then you can use it setting the correspondant binary

```php
<?php

use Knp\Snappy\Pdf;

$myProjectDirectory = '/path/to/my/project';

$snappy = new Pdf($myProjectDirectory . '/vendor/bin/wkhtmltopdf');
```
**Note**: You must use the version that corresponds to you.
`wkhtmltopdf` for unix systems
`wkhtmltopdf64.exe` for windows.


## Maintainers

If you are interested in maintaining, feel free to open a PR.
This library is maintained by the following people (alphabetically sorted) :

@ciltocruz

## Credits

This library has been originally developed by [ciltocruz](https://twitter.com/ciltocruz).
